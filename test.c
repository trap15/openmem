/*
	openmem -- a free open-source memory manager written in C
	Test

Copyright (C) 2010-2016 Alex "trap15" Marshall <trap15@raidenii.net>

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "openmem.h"

#define ARENA_SIZE  (0x100000)
#define CHUNK_COUNT (0x001000)

typedef struct {
	void* mem;
	size_t size;
} chunk_t;

lock_cookie_t openmem_take_lock(void) { return 0; }
void openmem_give_lock(lock_cookie_t cookie) { (void)cookie; }

int main(int argc, char *argv[])
{
	void* chunk;
	chunk_t chunks[CHUNK_COUNT];
	int i, idx;

	printf("Openmem test\n");
	printf("Allocating arena\n");
	chunk = malloc(ARENA_SIZE);
	if(chunk == NULL) {
		perror("Error allocating arena");
		goto end;
	}
	printf("Adding arena from %p\n", chunk);
	openmem_add_chunk((uintptr_t)chunk, ARENA_SIZE);
	openmem_stats();

	printf("Allocating too much...\n");
	chunks[0].size = ARENA_SIZE*2;
	chunks[0].mem = openmem_malloc(chunks[0].size);
	openmem_stats();
	if(chunks[0].mem != NULL) {
		printf("Still allocated!?\n");
		openmem_stats();
		goto end;
	}

	printf("Allocating random sizes...\n");
	srand(0x100);
	for(i = 0; i < CHUNK_COUNT; i++) {
		chunks[i].size = (rand() % (ARENA_SIZE / (CHUNK_COUNT * OPENMEM_BLOCK_SIZE))) + 1;
		chunks[i].mem = openmem_calloc(chunks[i].size, 1);
		if(chunks[i].mem == NULL) {
			printf("Unable to allocate %d: %zX\n", i, chunks[i].size);
			openmem_stats();
			goto end;
		}
	}

	printf("Freeing in random order...\n");
	for(i = 0; i < CHUNK_COUNT;) {
		idx = rand() % CHUNK_COUNT;
		if(chunks[idx].mem != NULL) {
			openmem_free(chunks[idx].mem);
			chunks[idx].mem = NULL;
			i++;
		}
	}
	openmem_stats();
	printf("If there's more than one free chunk here, we failed.\n");
end:
	free(chunk);
	return 0;
}

