OBJECTS = openmem.o
OUTPUT = libopenmem.a
INCLUDE = -I./
CFLAGS ?= -std=c99 -Wall -Wno-format -O2 -g -pedantic
CFLAGS += $(INCLUDE)
INSTALL_DIR ?= /usr/local

.PHONY: all install clean

all: $(OUTPUT) test
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(AR) rcs $(OUTPUT) $(OBJECTS)
test: $(OUTPUT) test.o
	$(CC) $(OBJECTS) test.o -o test
install: $(OUTPUT)
	install -c $(OUTPUT) $(INSTALL_DIR)/lib/$(OUTPUT)
	install -c -m 644 openmem.h $(INSTALL_DIR)/include/openmem.h
clean:
	rm -f $(OUTPUT) $(OBJECTS) test test.o

