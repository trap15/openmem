/*
	openmem -- a free open-source memory manager written in C
	openmem code

Copyright (C) 2010-2016 Alex "trap15" Marshall <trap15@raidenii.net>

*/

#include "openmem.h"

typedef struct _openmem_memlist {
	struct _openmem_memlist* prev;
	struct _openmem_memlist* next;
	uint32_t valid;
	size_t size;	/* Size of payload (without tag) */
} _openmem_memlist;

/* Gets the _openmem_memlist for a pointer */
#define OPENMEM_LISTDATA(x)		((_openmem_memlist*)((char*)(x) - sizeof(_openmem_memlist)))
/* Gets the address of the data for the memlist */
#define OPENMEM_PTRADDR(x)		((uintptr_t)((char*)(x) + sizeof(_openmem_memlist)))
/* Magic for memlists to attempt to curb memory fuckups */
#define OPENMEM_VALID_MAGIC		(0xDEADFACE)

_openmem_memlist* _openmem_used = NULL;
_openmem_memlist* _openmem_unused = NULL;

static int prv_allocate_memory(_openmem_memlist** ent, size_t size)
{
	_openmem_memlist* newent = NULL;
	_openmem_memlist* entry = *ent;
	/* If the size we want is less than the payload minus an entry... */
	/* We check this because if we can split up the entry, we will. */
	/* End result change would be:
		[entry][   m  e  m  o  r  y   ]
		[entry][memory][newent][newmem]
	*/
	if(OPENMEM_ROUNDUP_MEMSIZE(size + sizeof(_openmem_memlist)) < entry->size) {
		newent = (_openmem_memlist*)(OPENMEM_PTRADDR(entry) + size);
		newent->next = entry->next;
		newent->prev = entry->prev;
		newent->size = entry->size - size - sizeof(_openmem_memlist);
		newent->valid = OPENMEM_VALID_MAGIC;
		entry->size = size;
		if(entry->next)
			entry->next->prev = newent;
		if(entry->prev)
			entry->prev->next = newent;
	/* If this chunk isn't the exact size we want and we can't split it, just ditch this entry */
	}else if(size != entry->size) {
		return 0;
	}else{
		if(entry->prev)
			entry->prev->next = entry->next;
		if(entry->next)
			entry->next->prev = entry->prev;
	}

	if(_openmem_used)
		_openmem_used->prev = entry;
	entry->prev = NULL;
	entry->next = _openmem_used;
	_openmem_used = entry;
	if(newent)
		*ent = newent;
	return 1;
}

void* openmem_malloc(size_t size)
{
	lock_cookie_t cookie;
	void* mem;
	_openmem_memlist* list;

	if(size == 0)
		return NULL;

	mem = NULL;
	cookie = openmem_take_lock();

	size = OPENMEM_ROUNDUP_MEMSIZE(size);
	if(size < _openmem_unused->size) { /* Have to do a special case */
		mem = (void*)OPENMEM_PTRADDR(_openmem_unused);
		if(prv_allocate_memory(&_openmem_unused, size))
			goto donealloc;
		else
			mem = NULL;
	}
	for(list = _openmem_unused->next; list != NULL; list = list->next) {
		if(size <= list->size) {
			mem = (void*)OPENMEM_PTRADDR(list);
			if(prv_allocate_memory(&list, size))
				break;
			else
				mem = NULL;
		}
	}
donealloc:
	openmem_give_lock(cookie);
	return mem;
}

void* openmem_calloc(size_t count, size_t size)
{
	void *mem;
	size_t total = count * size;
	/* total will only overflow if either count or size have any of the top half of bits set */
#define HALF_SIZE_T (1UL << (sizeof(size_t) * 8 / 2))
	if((count | size) >= HALF_SIZE_T) {
		if(count == 0 || total / count != size) /* size overflow */
			return NULL;
	}
	mem = openmem_malloc(total);
	if(mem == NULL)
		return NULL;
	memset(mem, 0, total);
	return mem;
}

void* openmem_realloc(void* ptr, size_t size)
{
	void* mem;
	lock_cookie_t cookie;
	size_t copysize;

	cookie = openmem_take_lock();

	mem = openmem_malloc(size);
	if(mem == NULL)
		return NULL;

	if(size > OPENMEM_LISTDATA(ptr)->size)
		copysize = OPENMEM_LISTDATA(ptr)->size;
	else
		copysize = size;

	memcpy(mem, ptr, copysize);
	openmem_free(ptr);

	openmem_give_lock(cookie);
	return mem;
}

static void prv_condense_two_entries(_openmem_memlist* list1, _openmem_memlist* list2)
{
	list1->size += list2->size + sizeof(_openmem_memlist);
	if(list2 == _openmem_unused) {
		_openmem_unused = list2->next;
	}
	if(list2->next)
		list2->next->prev = list2->prev;
	if(list2->prev)
		list2->prev->next = list2->next;
	list2->valid = 0;
}

static void prv_condense_list_entries(void)
{
	_openmem_memlist* list;
	/* Merge entries */
	for(list = _openmem_unused; list->next != NULL;) {
		if((list->size + (uintptr_t)list + sizeof(_openmem_memlist)) == (uintptr_t)list->next) {
			prv_condense_two_entries(list, list->next);
			continue;
		}
		list = list->next;
	}
}

static void prv_unused_insert(_openmem_memlist* list, _openmem_memlist* ent)
{
	ent->next = list;
	ent->prev = list->prev;
	if(list->prev)
		list->prev->next = ent;
	list->prev = ent;
	if(list == _openmem_unused) {
		_openmem_unused = ent;
	}
}

static void prv_unused_append(_openmem_memlist* list, _openmem_memlist* ent)
{
	ent->prev = list;
	list->next = ent;
}

void openmem_free(void* ptr)
{
	lock_cookie_t cookie;
	_openmem_memlist* list;
	_openmem_memlist* unused;
	_openmem_memlist* last_unused;

	cookie = openmem_take_lock();
	if(ptr == NULL) { /* Invalid pointer */
		goto _done;
	}
	list = OPENMEM_LISTDATA(ptr);
	if(list->valid != OPENMEM_VALID_MAGIC) { /* Invalid pointer */
		goto _done;
	}
	if(list->prev)
		list->prev->next = list->next;
	if(list->next)
		list->next->prev = list->prev;
	if(list == _openmem_used)
		_openmem_used = list->next;
	list->prev = NULL;
	list->next = NULL;

	/* Insert in memory order */
	last_unused = NULL;
	for(unused = _openmem_unused; unused != NULL; unused = unused->next) {
		last_unused = unused;
		if((uintptr_t)list < (uintptr_t)unused) {
			prv_unused_insert(unused, list);
			break;
		}
	}
	if(unused == NULL) {
		if(last_unused)
			prv_unused_append(last_unused, list);
		else
			_openmem_unused = list;
	}

	prv_condense_list_entries();

_done:
	openmem_give_lock(cookie);
}

void openmem_add_chunk(uintptr_t addr, size_t size)
{
	_openmem_memlist* entry;
	size_t diff;
	diff = OPENMEM_BLOCK_SIZE - (addr % OPENMEM_BLOCK_SIZE);
	if(diff >= size) /* Not big enough when aligned */
		return;

	size -= diff;
	size = OPENMEM_ROUNDDOWN_MEMSIZE(size);
	if(size == 0)
		return;

	addr = OPENMEM_ROUNDUP_MEMSIZE(addr);
	entry = (_openmem_memlist*)addr;
	entry->size = size - sizeof(_openmem_memlist);

	if(_openmem_unused)
		_openmem_unused->prev = entry;
	entry->valid = OPENMEM_VALID_MAGIC;
	entry->prev = NULL;
	entry->next = _openmem_unused;
	_openmem_unused = entry;
}

#if OPENMEM_STATS_ALLOWED
#include <stdio.h>
#include <inttypes.h>
void openmem_stats(void)
{
	_openmem_memlist* list;
	int i;
	printf("Free list\n==========\n");
	for(i = 0, list = _openmem_unused; list != NULL; list = list->next, i++) {
		printf("%08X: %"PRIXPTR"~%"PRIXPTR" (n: %p p: %p)\n", i,
					OPENMEM_PTRADDR(list), OPENMEM_PTRADDR(list) + list->size,
					list->next, list->prev);
	}
	printf("    %4d Free chunks.\n", i);
	printf("Used list\n==========\n");
	for(i = 0, list = _openmem_used; list != NULL; list = list->next, i++) {
		printf("%08X: %"PRIXPTR"~%"PRIXPTR" (n: %p p: %p)\n", i,
					OPENMEM_PTRADDR(list), OPENMEM_PTRADDR(list) + list->size,
					list->next, list->prev);
	}
	printf("    %4d Used chunks.\n\n", i);
}
#endif
