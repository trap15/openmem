/*
	openmem -- a free open-source memory manager written in C
	openmem header

Copyright (C) 2010-2016 Alex "trap15" Marshall <trap15@raidenii.net>

*/

/*! \file */

#ifndef OPENMEM_H_
#define OPENMEM_H_

#include <stdlib.h> /* For NULL */
#include <stdint.h> /* For size_t and uintptr_t */
#include <string.h> /* For memcpy and memset */

/* You probably only want this for debugging, relies on printf */
#define OPENMEM_STATS_ALLOWED 1

/* Help prevent too much memory fragmentation */
#define OPENMEM_BLOCK_SIZE (0x40) /*!< Block size. All chunks are aligned to this */
#define OPENMEM_ROUNDUP_MEMSIZE(x) \
  ((size_t)(x) + (OPENMEM_BLOCK_SIZE - ((size_t)(x) % OPENMEM_BLOCK_SIZE)))
#define OPENMEM_ROUNDDOWN_MEMSIZE(x) \
  ((size_t)(x) - ((size_t)(x) % OPENMEM_BLOCK_SIZE))


/* PORTING: Might want to change this based on platform */
typedef uint32_t lock_cookie_t;

/*! \brief Take memory management lock
 *
 * Takes the memory management lock.
 * Should work recursively in order for realloc to work.
 *
 * \return Any data that needs to be passed to openmem_give_lock()
 * \sa openmem_give_lock()
 */
lock_cookie_t openmem_take_lock(void); /* PORTING: Implement this. */
/*! \brief Give memory management lock
 *
 * Gives back the memory management lock.
 * Should work recursively in order for realloc to work.
 *
 * \param cookie data returned from openmem_take_lock()
 * \sa openmem_take_lock()
 */
void openmem_give_lock(lock_cookie_t cookie); /* PORTING: Implement this. */

/* Setup */
/*! \brief Adds a chunk of memory to the pool
 * \param addr address of memory chunk
 * \param size side of memory chunk
 */
void openmem_add_chunk(uintptr_t addr, size_t size);

/* Functions */
/*! \brief Allocates memory
 * \param size size of memory to allocate
 * \return NULL on an error, or a pointer to the memory
 * \sa openmem_calloc(), openmem_realloc(), openmem_free()
 */
void* openmem_malloc(size_t size);
/*! \brief Allocates and clears memory
 *
 * \param count number of elements to allocate
 * \param size size of each element to allocate
 * \return NULL on an error, or a pointer to the memory
 * \sa openmem_malloc(), openmem_realloc(), openmem_free()
 */
void* openmem_calloc(size_t count, size_t size);
/*! \brief Extends the size of a pointer, keeping as much data as possible
 * \param ptr the original pointer returned by malloc to free
 * \param size new size of memory
 * \return NULL on an error, or a pointer to the memory
 * \sa openmem_malloc(), openmem_calloc(), openmem_free()
 */
void* openmem_realloc(void* ptr, size_t size);
/*! \brief Frees a pointer
 * \param ptr the original pointer returned by malloc to free
 * \sa openmem_malloc(), openmem_calloc(), openmem_realloc()
 */
void openmem_free(void* ptr);

#if OPENMEM_STATS_ALLOWED
/*! \brief Prints used and free stats
 */
void openmem_stats(void);
#endif

#endif

